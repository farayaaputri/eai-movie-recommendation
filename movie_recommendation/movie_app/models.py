from django.db import models

# Create your models here.
class Genre(models.Model):
    name = models.CharField(max_length=500)

    class Meta:
        verbose_name = "Genre"
        verbose_name_plural = "Genres"
    def __str__(self):
        return self.name

class Movie(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    year = models.CharField(max_length=4)
    imdb_score = models.FloatField()
    genre = models.ManyToManyField(Genre)
    image = models.FilePathField(path="/img/default.jpg")
    # rating = models.CharField(max_length=4, default='0%')
    class Meta:
        verbose_name = "Movie"
        verbose_name_plural = "Movies"
    def __str__(self):
        return self.name

